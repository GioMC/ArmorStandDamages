package fr.japanes;

import fr.japanes.events.DamageEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Gio on 24/12/2017.
 */
public class ArmorStandDamages extends JavaPlugin{

    public void onEnable(){
        getServer().getPluginManager().registerEvents(new DamageEvent(), this);
    }

}
