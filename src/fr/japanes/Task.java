package fr.japanes;

import fr.japanes.armorstand.CreateArmorStand;
import org.bukkit.entity.ArmorStand;

/**
 * Created by Gio on 24/12/2017.
 */
public class Task extends Thread{

    ArmorStand place;

    public Task(ArmorStand armorStand){
        this.place = armorStand;
    }

    public void run(){
        try {
            Thread.sleep(250);
            place.remove();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
