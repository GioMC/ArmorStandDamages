package fr.japanes.armorstand;

import fr.japanes.Task;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;

/**
 * Created by Gio on 24/12/2017.
 */
public class CreateArmorStand {

    public CreateArmorStand(double i, Player player){
        Location location = player.getLocation();
        Location finalLocation = location;
        finalLocation.add(new Vector(0.25,0.45,0));
        ArmorStand armorStand = (ArmorStand) Bukkit.getWorld("world").spawnEntity(new Location(Bukkit.getWorld("world"), finalLocation.getX(), finalLocation.getY(), finalLocation.getZ()), EntityType.ARMOR_STAND);
        armorStand.setGravity(false);
        armorStand.setBasePlate(false);
        armorStand.setVisible(false);
        armorStand.setCustomName(i+"");
        armorStand.setCustomNameVisible(true);
        Task task = new Task(armorStand);
        task.start();


        System.out.println(player + " " +i);
    }



}
