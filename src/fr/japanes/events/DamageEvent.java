package fr.japanes.events;

import fr.japanes.armorstand.CreateArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Gio on 24/12/2017.
 */
public class DamageEvent implements Listener{

    @EventHandler
    public void onDamageEvent(EntityDamageEvent event){
        if(!(event.getEntity() instanceof Player)){
            return;
        }
        Player player = (Player)event.getEntity();
        double i = event.getDamage();
        i = Math.round(i);
        i = i /2;
        CreateArmorStand createArmorStand = new CreateArmorStand(i, player);
    }

}
